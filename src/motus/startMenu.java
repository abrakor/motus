package motus;

import javax.swing.*;
import java.io.IOException;

public class startMenu extends MainFrame {
    private JPanel panel1;
    private JButton button1;

    private JButton button2;
    private JRadioButton defaultRadioButton;
    private JRadioButton CustomRadioButton;
    private JButton quitterButton;

    public startMenu(){

        setContentPane(panel1);

        button1.addActionListener(actionEvent -> {
            try {
                MainFrame.start(isDefault());
                setVisible(false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        button2.addActionListener(e -> {
            try {
                ListMenu.start();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
            setVisible(false);
        });

        quitterButton.addActionListener(e -> {
            System.exit(0);
        });
    }

    public boolean isDefault(){
        return defaultRadioButton.isSelected();
    }

    public static void main(String[] args) {
        startMenu startMenu = new startMenu();
        startMenu.setSize(500,500);
        startMenu.setResizable(false);
        startMenu.setLocationRelativeTo(null);
        startMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
