package motus;


import java.awt.font.TextAttribute;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class Word {

    protected String wordToGuess;
    protected String wordHidden;
    protected String wordFound;
    protected int size;

    private final int[] letters = new int[26];
    private final int[] correctLetters = new int[26];

    private static final String REDBALISE = "<FONT COLOR=RED>";
    private static final String BLACKBALISE = "<FONT COLOR=BLACK>";
    private static final String YELLOWBALISE = "<FONT COLOR=YELLOW>";
    private static final String NOFONT = "</FONT>";

    public Word(String wordToGuess) {
        resetarray();
        this.wordToGuess = wordToGuess;
        this.size = wordToGuess.length();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++){
            if (i == 0) {
                sb.append(wordToGuess.charAt(i));
            }
            else {
                sb.append("-");
            }
        }
        this.wordHidden = sb.toString();
        for (int i = 0; i < size; i++) {
            letters[wordToGuess.charAt(i) - 'a']++;
        }
    }

    private int[] verifyWord(String word) {
        int[] res = correctLetters;
        for (int i =0; i < 26; i++)
            res[i] = 0;
        if (word.equals(wordToGuess)) {
            return letters;
        }
        for (int i = 0; i < size; i++) {
            if (word.charAt(i) == wordToGuess.charAt(i)) {
                res[word.charAt(i) - 'a']++;
            }
        }
        return res;
    }

    public boolean isWordInFile(String word) throws IOException {
        //return true if  word is in the file src/motus/words.txt
        String[] words = Files.readAllLines(Paths.get("src/motus/words.txt")).toArray(new String[0]);
        String[] words2 = Files.readAllLines(Paths.get("src/motus/customWords.txt")).toArray(new String[0]);

        for (String s : words2) {
            if (s.equals(word)) {
                return true;
            }
        }
        for (String s : words) {
            if (s.equals(word)) {
                return true;
            }
        }
        return false;
    }


    public void isWordFound(String word) {
        StringBuilder builder = new StringBuilder("<html>");
        int[] tmp = verifyWord(word);
        for (int i = 0; i < word.length(); i++) {
            if ((word.charAt(i) != '-') && (word.charAt(i) == wordToGuess.charAt(i))) {

                builder.append(REDBALISE + word.charAt(i) + NOFONT);
            } else if ((word.charAt(i) != '-') && (word.charAt(i) != wordToGuess.charAt(i)) && (tmp[word.charAt(i) - 'a'] < letters[word.charAt(i) - 'a']))
            {
                builder.append(YELLOWBALISE + word.charAt(i) + NOFONT);
                tmp[word.charAt(i) - 'a']++;
            } else
                builder.append(BLACKBALISE).append(word.charAt(i)).append(NOFONT);
        }
        builder.append(NOFONT + "</html>");
        wordFound = builder.toString();
    }

    private void resetarray() {
        for (int i = 0; i < 26; i++) {
            correctLetters[i] = 0;
            letters[i] = 0;
        }
    }

    public String getWordToGuess() {
        return wordToGuess;
    }

    public void setWordToGuess(String wordToGuess) {
        this.wordToGuess = wordToGuess;
    }

    public String getWordHidden() {
        return wordHidden;
    }

    public String getWordFound() {
        return wordFound;
    }
}
