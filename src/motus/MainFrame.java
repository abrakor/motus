package motus;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.font.TextAttribute;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class MainFrame extends JFrame {
    private JButton button1;
    private JLabel try1, try2, try3, try4, try5, try6, Title, currentSituation;
    private JPanel mainPanel, panel;
    private JButton button2;
    private JButton retourAuMenuButton;
    private final JLabel[] labels = new JLabel[]{try1, try2, try3, try4, try5, try6};
    private int tests;
    private int currentPos;
    private static Word wordToGuess = new Word("");
    private boolean isDefault;

    public MainFrame() {
        tests = 0;
        currentPos = 1;
        setContentPane(mainPanel);
        setVisible(true);
        setSpace();
        try1.setText(wordToGuess.getWordHidden());
        mainPanel.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {}

            @Override
            public void keyPressed(KeyEvent keyEvent) {
                ///If the key pressed is typed check the result
                if (keyEvent.getKeyCode() == KeyEvent.VK_ENTER) {
                    verify();
                }
                ///if the return key is pressed, delete the last letter
                else if (keyEvent.getKeyCode() == 0x08) {
                    deleteLetter();
                }
                //if the key pressed is a minus, print it in the label
                else if ((keyEvent.getKeyCode() >= 65 && keyEvent.getKeyCode() <= 90)) {
                    updateLabel(keyEvent);
                }
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {}
        });

        button1.addActionListener(e -> {
            try {
                restart(MainFrame.this);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        button2.addActionListener(e -> {
            verify();
        });

        retourAuMenuButton.addActionListener(e -> {
            setVisible(false);
            startMenu.main(null);
        });
    }

    public void setSpace()
    {
        for (var label : labels){
            Map<TextAttribute, Object> attribute = new HashMap<>();
            attribute.put(TextAttribute.TRACKING, 0.5);
            label.setFont(label.getFont().deriveFont(attribute));
        }
    }

    /*
    The next 3 functions are used to deal with the keyboard events
     */


    //If the verify button is clicked or the enter key is pressed
    public void verify() {
        currentPos = 1;
        String verify = labels[tests].getText().toLowerCase();
        if (verify.length() != wordToGuess.getWordToGuess().length())
            return;
        if (tests > 5) {
            Title.setText("LOST");
        } else {
            try {
                if (wordToGuess.isWordInFile(verify)) {
                    if (!verify.equals(wordToGuess.getWordToGuess())) {
                        labels[tests + 1].setText(wordToGuess.getWordHidden());
                    }
                    wordToGuess.isWordFound(verify);
                    String actual = wordToGuess.getWordFound();
                    labels[tests].setText(actual);
                    tests++;
                    if (verify.equals(wordToGuess.getWordToGuess())) {
                        currentSituation.setForeground(Color.GREEN);
                        currentSituation.setText("Congratulations the word was: " + wordToGuess.getWordToGuess());
                    } else {
                        currentSituation.setForeground(Color.RED);
                        currentSituation.setText("Wrong, try again");
                    }
                } else {
                    labels[tests].setText(wordToGuess.getWordHidden());
                    currentPos = 1;
                }
            } catch (IOException f) {
                throw new RuntimeException(f);
            }
        }
    }

    //If the delete key is pressed then delete the previous letter
    public void deleteLetter() {
        StringBuilder stringBuilder = new StringBuilder();
        String compare = labels[tests].getText();
        int len = compare.length();
        for (int i = 0; i < len - 1; i++) {
            if ((compare.charAt(i + 1) == '-' && i != 0 && i == currentPos - 1)) {
                currentPos -= 1;
                stringBuilder.append("-");
            } else {
                stringBuilder.append(compare.charAt(i));
            }
            labels[tests].setText(stringBuilder.toString());
        }
        if (compare.charAt(len - 1) != '-') {
            currentPos -= 1;
            stringBuilder.append("-");

        } else {
            stringBuilder.append(compare.charAt(len - 1));
        }
        labels[tests].setText(stringBuilder.toString());

    }

    //If any key from a to z is pressed then update the label
    public void updateLabel(KeyEvent keyEvent) {
        //create the stringbuilder to print the word
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < wordToGuess.size; i++) {
            //if we are in the current position, print the letter
            if (currentPos == i)
                stringBuilder.append(keyEvent.getKeyChar());
                //when we are not in the current position, print the label
            else if (i < currentPos)
                stringBuilder.append(labels[tests].getText().charAt(i));
                //otherwise, print the dash
            else
                stringBuilder.append('-');
        }
        currentPos++;
        String a = stringBuilder.toString();
        labels[tests].setText(a);
    }


    //retourne le mot à deviner
    public static String wordToGuess(boolean def) throws IOException {
        //returns a random line of the file src/motus/words
        String[] words;
        if (def)
            words = Files.readAllLines(Paths.get("src/motus/words.txt")).toArray(new String[0]);
        else
            words = Files.readAllLines(Paths.get("src/motus/customWords.txt")).toArray(new String[0]);
        return words[(int) (Math.random() * words.length)];
    }

    /*
    The two next functions deals with either starting or restarting the game
     */

    public void restart(MainFrame frame) throws IOException {
        for (int i = 0; i < 6; i++)
            frame.labels[i].setText("");

        wordToGuess = new Word(wordToGuess(isDefault));
        System.out.println(wordToGuess.getWordToGuess());
        frame.tests = 0;
        frame.currentPos = 1;
        frame.labels[0].setText(wordToGuess.getWordHidden());
        //return wordToGuess.getWordToGuess();
    }

    //DO NOT USE ! - Simply a function to deal with the basic word list
   /* public static void epure() throws IOException {

        File file = new File("src/motus/liste.txt");
        FileWriter writer = new FileWriter(file);
        FileReader fr = new FileReader("src/motus/words.txt");   //reads the file
        BufferedReader br = new BufferedReader(fr);  //creates a buffering character input stream
        String line;
        while ((line = br.readLine()) != null) {
            int size = line.length();
            boolean add = true;
            if (size > 3 && size < 10) {
                int stupid = 0;
                for (int i = 0; i < size; i++) {
                    if ((line.charAt(i) >= 'a' && line.charAt(i) <= 'z')) {
                        stupid++;
                    } else
                        add = false;
                }
            } else
                add = false;
            if (add)
                writer.write(line + "\n");
        }
        writer.close();
        fr.close();
    }*/

    public static void start( boolean def) throws IOException {
        wordToGuess = new Word(wordToGuess(def));
        wordToGuess.setWordToGuess(wordToGuess.getWordToGuess().toLowerCase());
        System.out.println(wordToGuess.getWordToGuess());
        MainFrame frame = new MainFrame();
        frame.isDefault = def;
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int height = screenSize.height;
        int width = screenSize.width;
        frame.setSize(width / 2, height / 2);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}