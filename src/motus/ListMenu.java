package motus;

import javax.swing.*;
import java.awt.*;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;


public class ListMenu extends JFrame{
    private JPanel panel1;
    private JButton buttonAddInList;
    private JTextField textField1;
    private JButton supprimerLaListeButton;
    private JLabel label1;
    private JButton retourAuMenuButton;

    public ListMenu() throws IOException {
        printList();
        setVisible(true);
        setContentPane(panel1);
        buttonAddInList.addActionListener(e -> {
            try {
                addWordInFile();
                textField1.setText("");
                printList();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        supprimerLaListeButton.addActionListener(e -> {
            removeAll();
            try {
                printList();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        retourAuMenuButton.addActionListener(e -> {
            setVisible(false);
            startMenu.main(null);
        });
    }

    //add word in customWords.txt
    public void addWordInFile() throws IOException {
        //add the word in textField1 to src/motus/customWords.txt
        String word = textField1.getText();
        if ("src/motus/customWords.txt".contains(word)) {
            return;
        }
        try {
            FileWriter fw = new FileWriter("src/motus/customWords.txt", true);
            //Check whether the file is empty or not
            if (Files.size(Path.of("src/motus/customWords.txt")) == 0) {
                fw.write(word);
                fw.close();
            } else {
                fw.write("\n" + word);
                fw.close();
                System.out.println("Writing the word: " + word);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    //remove all words in customWords.txt
    public void removeAll() {
        try {
            FileWriter fw = new FileWriter("src/motus/customWords.txt");
            fw.write("");
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //print all words in customWords.txt
    public void printList() throws IOException {
        //print the words in customWords.txt in label1
        Path path = Path.of("src/motus/customWords.txt");
        List<String> lines = Files.readAllLines(path);
        StringBuilder sb = new StringBuilder();
        int i = 0;
        int size = lines.size();
        for (String line : lines) {
            sb.append(line);
            if (i < size - 1) {
                sb.append(", ");
            }
            i++;
        }
        label1.setText(sb.toString());
    }

    public static void start() throws IOException {
        ListMenu listMenu = new ListMenu();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width = (int) screenSize.getWidth();
        int height = (int) screenSize.getHeight();
        listMenu.setSize(width / 2, height / 2);
        listMenu.setLocationRelativeTo(null);
        listMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
